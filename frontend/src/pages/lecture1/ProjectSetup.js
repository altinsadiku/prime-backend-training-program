/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import addMember from 'assets/images/lecture1/add_member.png';
import cloneInstruction from 'assets/images/lecture1/clone_instruction.png';
import cloneSourceTree from 'assets/images/lecture1/clone_new_source_tree.png';
import fork from 'assets/images/lecture1/fork_instruction.png';
import newRemote from 'assets/images/lecture1/new_remote.png';
import privateRepo from 'assets/images/lecture1/private_repo.png';
import remoteMaster from 'assets/images/lecture1/remote_master.png';
import visualCodeImport from 'assets/images/lecture1/vs_code_import.png';
import Code from "presentations/Code";
import Divider from "presentations/Divider";
import SimpleLink from "presentations/rows/SimpleLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Italic } from "presentations/Label";
const styles = ({ typography }) => ({
  root: {},
})


const webpackGlobal =
`npm install -g webpack-dev-server
npm install -g webpack
npm install -g webpack-cli`

class ProjectSetup extends React.Component {
  render() {
    const { classes, section } = this.props
    let setup = section.children[0]
    let running = section.children[1]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Project Setup
          <Typography>Follow the steps to get access and create your fork of the main repository.</Typography>
          <Divider />
        </Typography>
        <Typography id={setup.id} variant={'title'}>
          {setup.display}
        </Typography>
        <Typography variant={'p'}>
          <ol>
            <li>Open your browser and go to this link: <SimpleLink href="https://gitlab.com">https://gitlab.com</SimpleLink></li>
            <li>Use your previously created account to login</li>
            <li><label>Follow this link to go to the main project in gitlab (maintained by Agon Lohaj): <SimpleLink href="https://gitlab.com/agonlohaj/prime-backend-training-program">https://gitlab.com/agonlohaj/prime-backend-training-program</SimpleLink></label></li>
            <li>Click the fork button</li>
            <Typography variant={'p'}>
              <img src={fork}></img>
            </Typography>
            <li>Select your account name as the namespace/group to fork the project</li>
            <li>Add Agon Lohaj as Maintainer</li>
            <Typography variant={'p'}>
              <img src={addMember}></img>
            </Typography>
            <li>Make the project private. Only Agon and you will be able to see it</li>
            <Typography variant={'p'}>
              <img src={privateRepo}></img>
            </Typography>
          </ol>
        </Typography>

        <Typography variant={'p'}>
          Before we move on with downloading the repository locally, lets make sure that Source Tree is working properly and your are authenticated correctly! If you have used the "Sign in with Google option" or any other provider than you might need to do the following:
            <ol>
              <li>Go to your <SimpleLink href="https://gitlab.com/profile/personal_access_tokens">personal access tokens</SimpleLink> on Gitlab</li>
              <li>Create a new personal access token</li>
              <li>Use that token as your password at Source Tree</li>
            </ol>
        </Typography>

        <Typography variant={'p'}>
          Now in order to have the project locally, we will have to clone it using Source Tree. Follow the instructions listed here:
          <ol>
            <li>Go to your forked project page</li>
            <li>Click the clone icon</li>
            <Typography variant={'p'}>
              <img src={cloneInstruction}></img>
            </Typography>
            <li>Use the clone with HTTPS, copy the url</li>
            <li>Open Source tree on your local machine</li>
            <li>Go ahead and choose the File -> Clone/New option. You can also find it using the add button and going at the Clone Tab</li>
            <Typography variant={'p'}>
              <img src={cloneSourceTree}></img>
            </Typography>
            <li>Paste the link at the source path, that you obtained from step 3</li>
            <li>Choose a folder at your Documents/Training Program/Repository</li>
            <li>Click Clone</li>
          </ol>
        </Typography>

        <Typography variant={'p'}>
          Now lets track the remote changes that we will publish. These are the new lectures that you will get at the next session
          <ol>
            <li>From the "Repository" menu click "Add Remote"</li>
            <li>Name the remote to "Training"</li>
            <li>The URL/path will be the URL of the training repository located here: <SimpleLink href="https://gitlab.com/agonlohaj/prime-backend-training-program">https://gitlab.com/agonlohaj/prime-backend-training-program</SimpleLink></li>
            <li>Name the remote to "Training"</li>
            <Typography variant={'p'}>
              <img src={newRemote}></img>
            </Typography>
            <li>When we do the next session, use the "Pull" functionality from Source Tree to get the next lecture into your "Forked" repository</li>
            <Typography variant={'p'}>
              <img src={remoteMaster}></img>
            </Typography>
          </ol>
        </Typography>

        <Typography variant={'p'}>
          Now that you have the code locally, go ahead and open VS Code and import the project. Follow this image to do that:
        </Typography>
        <Typography variant={'p'}>
          <img src={visualCodeImport}></img>
        </Typography>

        <Typography id={running.id} variant={'title'}>
          {running.display}
        </Typography>
        <Typography variant={'p'}>
          Open another CommandPrompt or Terminal at the projects backend directory. Once your there run the following command to install all the project dependencies:
        </Typography>
        <Code>
          {`sbt compile`}
        </Code>
        <Typography variant={'p'}>
          After that is completed successfully, in order to run the Backend API, simply type
        </Typography>
        <Code>
          {`sbt run`}
        </Code>

        <Typography variant={'p'}>
          In order to run the Mongo Server, than do the following:
          <ol>
            <li>Make sure the folder exists: C:/data/db (Windows) or ~/data/db (Mac)</li>
            <li>Go to Mongo installation directory (example: C:\Program Files\MongoDB\Server\4.4\bin)</li>
            <li>Execute the mongod file</li>
          </ol>
          <Italic>Windows Tip: Add the directory to System Path variables if you want to make the "mongod" command available on your Command Prompt</Italic>
        </Typography>
        <Typography variant={'p'}>
          Open CommandPrompt or Terminal at the projects frontend directory. Once your there run the following command to install all the project dependencies:
        </Typography>
        <Code>
          {`npm install`}
        </Code>
        <Typography variant={'p'}>
          After that is completed successfully, in order to run the Front End Web Application, simply type
        </Typography>
        <Code>
          {`npm start`}
        </Code>
        <Typography variant={'p'}>
          Open your browser at: <SimpleLink href="http://localhost:8080">http://localhost:8080</SimpleLink> and enjoy!
        </Typography>

        <Typography variant={'p'} fontStyle={'italic'}>
          Check the following depending on your IDE:
          <ol>
            <li>
              Visual Studio Code: Run: "sbt eclipse" To generate the java files which Visual Studio can use to identify the project and work with it better.
              Visual Studio will prompt to automatically install the Java Plugin. However it requires the Java 11 JDK to properly work. <SimpleLink href="http://www.oracle.com/technetwork/java/javase/downloads/">Download</SimpleLink> Java JDK 11 and set the java.home setting on VS code to point to the Java 11 JDK.
            </li>
            <li>
              IntelliJ: Install the Play Framework Plugin
            </li>
          </ol>
        </Typography>
        <Typography variant={'p'} fontStyle={'italic'}>
          <br/>
          In case you are experiencing difficulties starting the front end application, try the following:
          <ol>
            <li>
              Run the Command Prompt as an administrator
            </li>
            <li>
              Run:
              <Code>
                {webpackGlobal}
              </Code>
            </li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(ProjectSetup)
