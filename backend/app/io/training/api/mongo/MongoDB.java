package io.training.api.mongo;

import akka.Done;
import akka.actor.CoordinatedShutdown;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.typesafe.config.Config;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import play.Logger;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.bson.codecs.pojo.Conventions.ANNOTATION_CONVENTION;

/**
 * Created by Agon on 09/08/2020.
 */
public abstract class MongoDB {
	protected static final int MAX_CONNECTION_IDLE_TIME = 5 * 60 * 1000;
	protected static final int MAX_CONNECTION_LIFETIME = 10 * 60 * 1000;
	protected static final int MAX_CONNECTIONS_PER_HOST = 5;
	protected final Config config;
	public static int BATCH_SIZE = 10000;
	private static Integer opened = 0;

	// dev and production env
	protected MongoClient mongo;
	private MongoDatabase database;

	public MongoDB(CoordinatedShutdown coordinatedShutdown, Config config) {
		this.config = config;
		BATCH_SIZE = config.getInt("batch_size");

		coordinatedShutdown.addTask(CoordinatedShutdown.PhaseServiceStop(), "shutting-down-mongo-connections", () -> {
			Logger.of(this.getClass()).debug("Shutting down mongo connections!");
			close();
			return CompletableFuture.completedFuture(Done.done());
		});
	}

	protected void close() {
		if (database != null) {
			database = null;
		}
		opened--;
		disconnect();
	}

	/**
	 * If you call it like this, make sure you close it soon
	 * @return
	 */
	public MongoDatabase getMongoDatabase() {
		synchronized (opened) {
			if (database == null) {
				database = this.connect();

				opened++;
				Logger.of(this.getClass()).debug("OPENED MONGO: " + opened);

				if(database == null) {
					Logger.of(this.getClass()).debug("Database doesn't exist");
				}
			}
			CodecProvider pojoCodecProvider =
					PojoCodecProvider.builder()
							.conventions(Collections.singletonList(ANNOTATION_CONVENTION))
							.register("io.training.models")
							.automatic(true).build();
			final CodecRegistry customEnumCodecs = CodecRegistries.fromCodecs();
			CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(), customEnumCodecs, fromProviders(pojoCodecProvider));

			return database.withCodecRegistry(pojoCodecRegistry);
		}
	}

	protected abstract MongoDatabase connect();

	public abstract void disconnect();


	/**
	 * Get mongo database
	 * @return
	 */
	public CompletableFuture<MongoDatabase> getMongoDatabaseAsync() {
		CompletableFuture<MongoDatabase> promise = new CompletableFuture<>();
		try {
			MongoDatabase connection = this.getMongoDatabase();
			promise.complete(connection);
		} catch (NullPointerException ex) {
			promise.completeExceptionally(ex);
		}
		return promise;
	}
	/**
	 * Save from closing connections, get mongo database
	 * @return
	 */
	public <T> CompletableFuture<T> supplyAsync(Function<MongoDatabase, CompletableFuture<T>> function) {
		CompletableFuture<MongoDatabase> promise = new CompletableFuture<>();
		try {
			MongoDatabase connection = this.getMongoDatabase();
			promise.complete(connection);
		} catch (NullPointerException ex) {
			promise.completeExceptionally(ex);
		}
		CompletableFuture<T> next = promise.thenCompose(function::apply);
		return next;
	}


}
