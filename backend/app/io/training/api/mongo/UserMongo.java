package io.training.api.mongo;

import akka.actor.CoordinatedShutdown;
import com.google.common.base.Strings;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.typesafe.config.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agon on 09/08/2020
 */
public final class UserMongo extends MongoDB {

	public UserMongo(CoordinatedShutdown coordinatedShutdown, Config config) {
		super(coordinatedShutdown, config);
	}

	@Override
	protected MongoDatabase connect() {
		String host = config.getString("mongo.host");
		String[] hosts = new String[0];
		if(!Strings.isNullOrEmpty(host)) {
			hosts = host.split(",");
		}
		String port = config.getString("mongo.port");

		String[] ports = new String[0];
		if(!Strings.isNullOrEmpty(port)) {
			ports = port.split(",");
		}
		String userDB = config.getString("mongo.database");
		String username = config.getString("mongo.user");
		String password = config.getString("mongo.password");

		String userAuthenticationDatabase = config.getString("mongo.auth_database");

		// connection pooling
		MongoClientOptions options = MongoClientOptions.builder()
				.connectionsPerHost(MAX_CONNECTIONS_PER_HOST)
				.maxConnectionLifeTime(MAX_CONNECTION_LIFETIME)
				.maxConnectionIdleTime(MAX_CONNECTION_IDLE_TIME)
				.build();

		MongoCredential credential = null;
		if (!Strings.isNullOrEmpty(username)) {
			credential = MongoCredential.createCredential(username,
					!Strings.isNullOrEmpty(userAuthenticationDatabase) ? userAuthenticationDatabase : userDB,
					password.toCharArray());
		}
		List<ServerAddress> addresses = new ArrayList<>();
		for (int i = 0; i < hosts.length; i++) {
			addresses.add(new ServerAddress(hosts[i], Integer.parseInt(ports[i])));
		}
		if (credential != null) {
			mongo = new MongoClient(addresses, credential, options);
		} else {
			mongo = new MongoClient(addresses, options);
		}
		return mongo.getDatabase(userDB);
	}

	@Override
	public void disconnect() {
		if (mongo == null) {
			return;
		}
		mongo.close();
	}
}
